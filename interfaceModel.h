#ifndef INTERFACE_MODEL_H
#define INTERFACE_MODEL_H

#include "chessboardModel.h"

/*
 @requires: rien
 @assigns: rien
 @ensures: vide le terminal pour avoir une interface propre*/
void clearTerminal();



/*
 @requires: rien;
 @assigns: rien;
 @ensures: vide le buffer de l'entré stdin*/
void clearBuffer();



/*
 @require: word est un tableau de char de taille size
 @assigns: word contient les éléments entré au clavier
 @ensures: retourne 1 si la saisi a été correcte, 0 sino*/
int readKeyboard(char* word, int size);



/*
 @require : la case (cd,ld) et (ca,la) sont valide et il y a une tour en (cd,ld)
 @assigns : rien
 @ensure : retourne 1 si le mouvement de la case (cd,ld) à (ca,la) est valide, 0 sinon*/
int isRookMove(chessboard, char cd, int ld, char ca, int la);



/*
 @require : la case (cd,ld) et (ca,la) sont valide et il y a un fou en (cd,ld)
 @assigns : rien
 @ensure : retourne 1 si le mouvement de la case (cd,ld) à (ca,la) est valide, 0 sinon*/
int isBishopMove(chessboard, char cd, int ld, char ca, int la);



/*
 @require : la case (cd,ld) et (ca,la) sont valide et il y a un roi en (cd,ld)
 @assigns : rien
 @ensure : retourne 1 si le mouvement de la case (cd,ld) à (ca,la) est valide, 0 sinon*/
int isKingMove(char cd, int ld, char ca, int la);



/*
 @require : la case (cd,ld) et (ca,la) sont valide et il y a une dame en (cd,ld)
 @assigns : rien
 @ensure : retourne 1 si le mouvement de la case (cd,ld) à (ca,la) est valide, 0 sinon*/
int isQueenMove(chessboard, char cd, int ld, char ca, int la);



/*
 @require : la case (cd,ld) et (ca,la) sont valide et il y a un pion en (cd,ld)
 @assigns : rien
 @ensure : retourne 2 si le mouvement de la case (cd,ld) à (ca,la) n'est pas un mouvement d'attaque, 3 si c'est un mouvement d'attaque, 0 si le mouvement n'est pas valide*/
int isPawnMove(chessboard, int hasAlreadyMove, char cd, int ld, char ca, int la);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si le mouvement de la case (cd,ld) à la case (ca,la) est valide, 0 sinon*/
int isKnightMove(char cd, int ld, char ca, int la);



/*
 @require : la case (cd,ld) et (ca,la) sont valide et il y a p en (cd,ld)
 @assigns : rien
 @ensures : retourne 1 si le mouvement de (cd,ld) à (ca,la) par p est valide, 0 sinon*/
int isMoveOK(chessboard chess, piece p, int hasAlreadyMove, char cd, int ld, char ca, int la);



/*
 @require : isSecondSquare = 1, 2 ou 0
 @assigns : *c et *l sont remplacés par les valeurs entrées par le joueur (colonne et ligne)
 @ensure : si isSecondSquare = 0 ou 2 : demande au joueur d'entrer des coordonnées, d'abandonner ou éventuellement de changer de case. si isSecondSquare = 1 : demande au joueur de choisir une action*/
int nextSquare(chessboard chess, char* c, int* l, int isSecondSquare);



/*
 @require : la case (c,l) est valide
 @assigns : rien
 @ensure : retourne 1 si la case (c,l) possède une piece appartenant au joueur dont c'est le tour*/
int isGoodPlayerSquare(chessboard chess, char c, int l);



/*
 @require : rien
 @assigns : rien
 @ensure : retourne 1 si la case (c,l) est valide,
                    -2 si la colonne est incorrecte
                    -3 si la ligne est incorrecte */
int isSquareOK(chessboard, char c, int l);



/*
 @requires: nb > 0
 @assigns: rien
 @ensures: retourne un nombre compris entre 1 et nbPiecesPremiereCase*/
int nb_de_pieces_a_deplacer(int nbPiecesPremiereCase);



/*
 @require : rien
 @assigns : rien
 @ensures : retourne 1 si l'offensive est possible, 0 sinon */
int isOffensiveOK(chessboard chess, char cd, int ld, char ca, int la, int nbPiecesPremiereCase);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si le joueur peut choisir la case (ca,la) comme case d'arrivée, 0 sinon*/
int isSecondSquareOK(chessboard chess, char cd, int ld, char ca, int la, int nbPiecesPremiereCase);



/*
 @requires: rien
 @assigns: vide *s, modifie tabB et tabN
 @ensures: vide la pile *s en diminuant les nombres de pieces contenus dans les cases de tabB et tabN*/
void takePieces(player pl, int tabB[], int tabN[], stack* s);



/*
 @requires: rien
 @assigns: chess, tabB, tabN
 @ensures: effectue les movements de pieces*/
void move(chessboard chess, char cd, int ld, char ca, int la, int nbPiecesADeplacer, int tabB[], int tabN[]);


/*
 @requires: rien
 @assigns: tabB, tabN
 @ensures: régit les interractions avec les joueurs*/
int chooseSquare(chessboard chess, int tabB[], int tabN[]);

#endif
