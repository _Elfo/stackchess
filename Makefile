CC = gcc
CFLAGS = -Wall -Wextra

all : jeux

%.o : %.c
	$(CC) $(CFLAGS) -ansi -c $<

listesChainees.o : listesChainees.h
chessboardModel.o : chessboardModel.h listesChainees.h
interfaceModel.o : interfaceModel.h listesChainees.h chessboardModel.h
main.o : chessboardModel.h interfaceModel.h

jeux : listesChainees.o chessboardModel.o interfaceModel.o main.o
	$(CC) $(CFLAGS) $^ -o $@

