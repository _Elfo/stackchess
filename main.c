/*
 Tirbisch Romain
 */

#include <stdio.h>
#include <stdlib.h>

#include "chessboardModel.h"
#include "interfaceModel.h"


/*
 @require : rien
 @assigns : rien
 @ensure : retourne un entier entre 6 et 26 choisi par les joueurs */
int chooseChessSize(void) {
    char buffer[4];
    int n;
    
    printf("\nChoisissez la dimension de l'échiquier (entre 6 et 26) : ");
    readKeyboard(buffer, 4);
    n = strtol(buffer, NULL, 10);
    if( n == 42 )
        printf("Ca serait un peu exagéré !\n");
    else if( n < 6 || 26 < n )
        printf("Cette taille n'est pas valide !\n");
    return n;
}



/*
 @require: tabB et tabN sont des tableaux de taille size
 @assigns: rien
 @ensures: retourne 0 s'il y a égalité, 1 si les blancs ont gagné, 2 si les noirs ont gagné et -1 si le jeu se poursuit*/
int isOver(int tabB[], int tabN[], int size) {
    int kB1 = 0;
    int kB2 = 0;
    int kB3 = 0;
    
    int kN1 = 0;
    int kN2 = 0;
    int kN3 = 0;
    
    int i;
    for(i = 0; i < size; i++)
    {
        if( i == 1 || i == 2 || i == 4 )
            kB1 += tabB[i];
        else if( i == 1 || i == 2 || i == 3 || i == 5 )
            kB2 += tabB[i];
        else if( tabB[0] != 0 )
            kB3 += tabB[i];
        
        if( i == 1 || i == 2 || i == 4 )
            kN1 += tabN[i];
        else if( i == 1 || i == 2 || i == 3 || i == 5 )
            kN2 += tabN[i];
        else if( tabN[0] != 0 )
            kN3 += tabN[i];
    }
    
    
    if( kB1 + kB2 + kB3 == 0 ) /*Les noirs ont gagnés*/
    {
        clearTerminal();
        printf("\nLes Noirs ont gagné !\n");
        return 2;
    }
    else if( kN1 + kN2 + kN3 == 0 ) /*Les blancs ont gagnés*/
    {
        clearTerminal();
        printf("\nLes blancs ont gagné !\n");
        return 1;
    }
    else if( (kB1 <= 1 && kB2 <= 1 && kB3 <= 1) && (kN1 <= 1 && kN2 <= 1 && kN3 <= 1) ) /*Egalité*/
    {
        clearTerminal();
        printf("\nEgalité !\n");
        return 0;
    }
    else
        return -1;
}


int main(void) {
    int n; /*taille du plateau*/
    int nbChooseSquare; /*entier renvoyé par chooseSquare(game);*/
    char wantNewGame = 'Y'; /*est-ce que le joueur veut recommencer une partie ?*/
    
    /*
     tab[0] = nombre de cavalier
     tab[1] = nombre de roi
     tab[2] = nombre de dame
     tab[3] = nombre de fou
     tab[4] = nombre de tour
     tab[5] = nombre de pion*/
    int tabB[6] = {0, 0, 0, 0, 0, 0};
    int tabN[6] = {0, 0, 0, 0, 0, 0};
    chessboard game;
    
    do
    {
        clearTerminal();
        do
        {
            n = chooseChessSize();
        } while ( n < 6 || 26 < n ); /*tant que n n'est pas compris entre 6 et 26 on demande au joueur d'entrer un nombre*/
        startGame(n, &game, tabB, tabN); /*initilisation du plateau de jeu et des piles*/
        do
        {
            nbChooseSquare = chooseSquare(game, tabB, tabN);
            if( nbChooseSquare == -1 ) /*abandon des blancs*/
            {
                clearTerminal();
                printf("\nVous avez abandonné ! Les Noirs ont gagné !!\n");
                break; /*ON sort de la boucle while*/
            }
            else if( nbChooseSquare == -2 )  /*abandon des noirs*/
            {
                clearTerminal();
                printf("\nVous avez abandonné ! Les Blancs ont gagné !!\n");
                break;
            }
            else if( game.tour == BLANC )
                game.tour = NOIR;
            else
                game.tour = BLANC;
        } while( isOver(tabB, tabN, 6) < 0 ); /*tant que isOver(...) = -1 le jeu continue*/
        
        do /*tant que le joueur n'a pas entré une lettre valide on lui demande d'entrer un caractère*/
        {
            char buffer[3];
            printf("\nVoulez-vous commencer une nouvelle partie ? y/n : ");
            readKeyboard(buffer, 3);
            wantNewGame = buffer[0];
        } while( wantNewGame != 'Y' && wantNewGame != 'y' && wantNewGame != 'N' && wantNewGame != 'n' );
        
        freeChessboard(&game, tabB, tabN, 6); /*On libère la mémoire*/
    } while( wantNewGame == 'Y' || wantNewGame == 'y' );
    
    return 0;
}
