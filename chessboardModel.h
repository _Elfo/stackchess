#ifndef CHESSBOARD_MODEL_H
#define CHESSBOARD_MODEL_H

#include "listesChainees.h"



typedef enum {
    BLANC, NOIR
} player;


typedef struct {
    stack **cases;
    player tour;
    int taille;
} chessboard;



/*
 @requires: la case (c,l) existe
 @assigns: rien
 @ensures: retourne un pointeur vers la pile des coordonnées (c,l)*/
stack* popCases(chessboard chess, char c, int l);



/*
 @requires: la case (c,l) existe
 @assigns: supprime le premier élément de la pile des coordonnées (c,l)
 @ensures: retourne la piece au sommet de la pile des coordonnées (c,l)*/
piece popPieceCases(chessboard chess, char c, int l);



/*
 @requires: la case (c,l) existe
 @assigns: rien
 @ensures: retourne la piece au sommet de la pile de coordonnées (c,l)*/
piece returnTopPieceCases(chessboard chess, char c, int l);



/*
 @requires: la case (c,l) existe et hasMove = 0 ou 1
 @assigns: chess
 @ensures: rejoute p au sommet de la pile de coordonnées (c,l)*/
void pushPieceCases(chessboard chess, piece p, int hasMove, char c, int l);



/*
 @requires: la case (c,l) existe
 @assigns: rien
 @ensures: retourne le nombre de pieces de la pile de coordonnées (c,l)*/
int nbPieces(chessboard chess, char c, int l);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si la pile de coordonnées (c,l) est vide, 0 sinon*/
int isEmptyCase(chessboard chess, char c, int l);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si la pile de coordonnées (c,l) contient des pièces blanches, 0 sinon*/
int isWhiteCase(chessboard chess, char c, int l);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si la pile de coordonnées (c,l) contient des pièces noirs, 0 sinon*/
int isBlackCase(chessboard chess, char c, int l);




/*
 @requires: rien
 @assigns: rien
 @ensures: affiche la suite de lettre de A à A + n*/
void printLetter(int n);



/*
 @requires: rien
 @assigns: rien
 @ensures: affiche des tirets de séparation */
void printSeparation(int n);



/*
 @requires: rien
 @assigns: rien
 @ensures: affiches les cases de la ligne i*/
void printCases(chessboard chess, int i);



/*
    @requires: rien
    @assigns: rien
    @ensures: affiche l'échiquier et éventuellement les pieces de la case (*c,*l) */
void printChess(chessboard chess, char *c, int *l);



/*
 @requires: 6 <= n et n <= 26
 @assigns: *c
 @ensures: cree une matrice de taille n*n et initialise le tour à Blanc */
void initChessboard(int n, chessboard *c);



/*
 @requires: 6 <= n et n <= 26
 @assigns: *echiquier
 @ensures: les pieces sont dans leur position initiale */
void startGame(int n, chessboard *echiquier, int tabB[], int tabN[]);



/*
 @requies: tabB et tabN sont de taille size
 @assigns: *c, tabB, tabN
 @ensures: libère la mémoire et réinitialise tabB et tabN à 0*/
void freeChessboard(chessboard* c, int tabB[], int tabN[], int size);

#endif
