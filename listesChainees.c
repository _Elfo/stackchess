#include <stdio.h>
#include <stdlib.h>

#include "listesChainees.h"


void printPiece(piece p) {
    switch (p) {
        case RB :
            printf("\033[34mRB");
            printf("\033[0m");
            break;
        case DB :
            printf("\033[34mDB");
            printf("\033[0m");
            break;
        case FB :
            printf("\033[34mFB");
            printf("\033[0m");
            break;
        case CB :
            printf("\033[34mCB");
            printf("\033[0m");
            break;
        case TB :
            printf("\033[34mTB");
            printf("\033[0m");
            break;
        case PB :
            printf("\033[34mPB");
            printf("\033[0m");
            break;
            
        case RN : printf("RN"); break;
        case DN : printf("DN"); break;
        case FN : printf("FN"); break;
        case CN : printf("CN"); break;
        case TN : printf("TN"); break;
        case PN : printf("PN"); break;
            
        default : printf("  ");
    }
}



void createStack(piece p, stack *s) {
    if (isEmpty(*s) != 1) {
        printf("Erreur la pile n'est pas vide\n");
        exit(1);
    }
    if (p != VIDE) {
        *s = (stack) malloc(sizeof(struct maillon));
        (*s)->summit = p;
        (*s)->next = NULL;
        (*s)->hasMove = 0;
    }
}



int isEmpty(stack s) {
    return (s == NULL);
}



void push(piece p, stack *s, int hasMove) {
    stack res;
    
    res = (stack) malloc(sizeof(struct maillon));
    
    res->summit = p;
    res->next = *s;
    if(hasMove == 1)
        res->hasMove = 1;
    else
        res->hasMove = 0;
    *s = res;
}



piece pop(stack *s) {
    stack tmp;
    piece p = (*s)->summit;
    
    tmp = *s;
    *s = (*s)->next;
    
    free(tmp);
    
    return p;
}



piece returnTop(stack s) {
    if (isEmpty(s))
        return VIDE;
    else
        return s->summit;
}



void printStack(stack s) {
    while ( isEmpty(s) == 0 ) {
        printPiece(s->summit);
        printf(" -> ");
        s = s->next;
    }
    printf("vide\n");
}



void clearStack(stack *s) {
    if(isEmpty(*s))
        return;
    
    stack tmp;
    
    while (*s != NULL) {
        tmp = *s;
        *s = (*s)->next;
        free(tmp);
    }
}



int isWhitePiece(stack s) {
    piece p = returnTop(s);
    switch (p)
    {
        case RB:
        case DB:
        case FB:
        case CB:
        case TB:
        case PB:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}



int isBlackPiece(stack s) {
    piece p = returnTop(s);
    switch (p) {
        case RN: return 1; break;
        case DN: return 1; break;
        case FN: return 1; break;
        case CN: return 1; break;
        case TN: return 1; break;
        case PN: return 1; break;
        default: return 0; break;
    }
}



int hasAlreadyMove(stack s) {
    if(isEmpty(s) == 1) {
        printf("\nErreur la pile est vide\n");
        exit(1);
    }
    return s->hasMove;
}

