#ifndef LISTES_CHAINEES_H
#define LISTES_CHAINEES_H

typedef struct maillon *stack;

typedef enum {
    RB, DB, FB, CB, TB, PB,
    RN, DN, FN, CN, TN, PN,
    VIDE
} piece;


struct maillon {
    piece summit;
    int hasMove;
    stack next;
};


/*
    @requires: rien
    @assigns: rien
    @ensures: affiche la piece */
void printPiece(piece);



/*
    @requires s est NULL
    @assigns s
    @ensures modifie s en ajoutant la piece p au sommet */
void createStack(piece p, stack *s);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si la pile s est vide, 0 sinon*/
int isEmpty(stack s);



/*
 @requires: rien
 @assigns: *s
 @ensures: ajoute p au sommet de la pile *s*/
void push(piece p, stack* s, int hasMove);



/*
 @requires: *s non vide
 @assigns: *s
 @ensures: retire le premier élément de la pile et le retourne*/
piece pop(stack* s);



/*
 @requires: s non vide
 @assigns: rien
 @ensures: retourne le premier élément de la pile sans le supprimer*/
piece returnTop(stack s);



/*
 @requires: rien
 @assigns: rien
 @ensures: affiches les éléments de la piles*/
void printStack(stack s);



/*
    @requires rien
    @assigns *s
    @ensures libère l'espace alloué */
void clearStack(stack *s);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si la pile contient des pieces blanches, 0 sinon*/
int isWhitePiece(stack);



/*
 @requires: rien
 @assigns: rien
 @ensures: retourne 1 si la pile contient des pieces noires, 0 sinon*/
int isBlackPiece(stack);



/*
 @requires: s non vide
 @assigns: rien
 @ensures: retourne 1 si la piece au sommet de la pile s a déjà bougé, 0 sinon*/
int hasAlreadyMove(stack s);

#endif
