#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "interfaceModel.h"
#include "listesChainees.h"


void clearTerminal() {
    printf("\e[1;1H\e[2J");
}



void clearBuffer() {
    int c = 0;
    while( c != '\n' && c != EOF )
    {
        c = getchar();
    }
}



int readKeyboard(char* word, int size) {
    char* result = fgets(word, size, stdin);
    
    if( result != NULL )  /*s'il n'y a pas d'erreur de saisie*/
    {
        char* position = strchr(word, '\n'); /*On recherche le caractère saut de ligne*/
        if( position == NULL ) /*Si on l'a pas trouvé c'est que l'on a saisie trop de caractère*/
            clearBuffer(); /*donc on vide le buffer*/
        return 1;
    }
    else
    {
        clearBuffer();
        return 0;
    }
}



int isRookMove(chessboard chess, char cd, int ld, char ca, int la) /*TOUR*/ {
    int i;
    if( cd != ca && ld != la ) /*pas la même ligne ni la même colonne*/
        return 0;
    else if( cd == ca ) /*la même colonne*/
    {
        if( la < ld )
        {
            for(i = 1; i < ld - la; i++)
                if( isEmptyCase(chess, cd, ld - i) != 1 ) /*s'il y a une piece sur le chemin*/
                    return 0;
            return 1;
        }
        else if( la > ld )/*la ligne d'arrivée est au dessus de la ligne de départ*/
        {
            for(i = 1; i < la - ld; i++)
                if( isEmptyCase(chess, cd, ld + i) != 1 )
                    return 0;
            return 1;
        }
        else /*la case d'arrivée est la même que la case de départ*/
            return 0;
    }
    else /*la même ligne*/
    {
        if( ca < cd )
        {
            for(i = 1; i < cd - ca; i++)
                if( isEmptyCase(chess, cd - i, ld) != 1 )
                    return 0;
            return 1;
        }
        else if( ca > cd )
        {
            for(i = 1; i < ca - cd; i++)
                if( isEmptyCase(chess, cd + i, ld) != 1 )
                    return 0;
            return 1;
        }
        else
            return 0;
    }
}



isBishopMove(chessboard chess, char cd, int ld, char ca, int la) {
    int i;
    if( abs(cd - ca) != abs(ld - la) ) /*il faut forcement le même décalage en valeur absulue entre les lignes et les colonnes*/
        return 0;
    else if( ca < cd && la < ld ) /*diagonale basse gauche*/
    {
        for(i = 1; i < cd - ca; i++)
            if( isEmptyCase(chess, cd - i, ld - i) != 1 )
                return 0;
        return 1;
    }
    else if( ca < cd && la > ld ) /*diagonale haute gauche*/
    {
        for(i = 1; i < cd - ca; i++)
            if( isEmptyCase(chess, cd - i, ld + i) != 1 )
                return 0;
        return 1;
    }
    else if( ca > cd && la < ld ) /*diagonale basse droite*/
    {
        for(i = 1; i < ca - cd; i++)
            if( isEmptyCase(chess, cd + i, ld - i) != 1 )
                return 0;
        return 1;
    }
    else if( ca > cd && la > ld ) /*diagonale haute droite*/
    {
        for(i = 1; i < ca - cd; i++)
            if( isEmptyCase(chess, cd + i, ld + i) != 1 )
                return 0;
        return 1;
    }
    else
        return 0; /*la case d'arrivée est la même que la case de départ*/
}



int isKingMove(char cd, int ld, char ca, int la) {
    /*la case cible a déjà été vérifiée*/
    if( abs(cd - ca) > 1 || abs(ld - la) > 1 ) /*la case cible est à plus d'une case de la case de départ*/
        return 0;
    else if( cd == ca && ld == la )
        return 0;
    else
        return 1;
}



int isQueenMove(chessboard chess, char cd, int ld, char ca, int la) {
    if( isRookMove(chess, cd, ld, ca, la) == 1 )
        return 1;
    else if( isBishopMove(chess, cd, ld, ca, la) == 1 )
        return 1;
    else
        return 0;
}



int isPawnMove(chessboard chess, int hasAlreadyMove, char cd, int ld, char ca, int la) {
    if( chess.tour == BLANC ) {
        if( la == ld + 1 && ca == cd ) /*le pion avance d'une seule case*/
            return 2; /*retourne 2 pour signaler que le mouvement n'est pas un mouvement d'attaque*/
        else if( la == ld + 2 && ca == cd && hasAlreadyMove == 0 ) /*le pion avance de deux cases*/ {
            if( isEmptyCase(chess, cd, ld + 1) == 1 )
                return 2;
            else
                return 0;
        }
        else if( la == ld + 1 && (ca == cd + 1 || ca == cd - 1) ) /*le pion attaque*/
            return 3; /*retourne 3 pour signaler que le mouvement n'est qu'un mouvement d'attaque*/
        else
            return 0;
    }
    else if( chess.tour == NOIR ) {
        if( la == ld - 1 && ca == cd ) /*le pion avance d'une case*/
            return 1;
        else if( la == ld - 2 && ca == cd && hasAlreadyMove == 0 ) /*le pion avance de deux cases*/{
            if( isEmptyCase(chess, cd, ld - 1) == 1 )
                return 2;
            else
                return 0;
        }
        else if( la == ld - 1 && (ca == cd + 1 || ca == cd - 1) ) /*le pion attaque*/
            return 3;
        else
            return 0;
    }
    else
        return 0;
}



int isKnightMove(char cd, int ld, char ca, int la) {
    if( (ca == cd + 1 || ca == cd - 1) && la == ld + 2 )
        return 1;
    else if( (ca == cd + 1 || ca == cd - 1) && la == ld - 2 )
        return 1;
    else if( (la == ld + 1 || la == ld - 1) && ca == cd + 2 )
        return 1;
    else if( (la == ld + 1 || la == ld - 1) && ca == cd - 2 )
        return 1;
    else
        return 0;
}



int isMoveLegal(chessboard chess, piece p, int hasAlreadyMove, char cd, int ld, char ca, int la) {
    switch (p) {
        case RB:
        case RN:
            return isKingMove(cd, ld, ca, la); break;
        case DB:
        case DN:
            return isQueenMove(chess, cd, ld, ca, la); break;
        case FB:
        case FN:
            return isBishopMove(chess, cd, ld, ca, la); break;
        case CB:
        case CN:
            return isKnightMove(cd, ld, ca, la); break;
        case TB:
        case TN:
            return isRookMove(chess, cd, ld, ca, la); break;
        case PB:
        case PN:
            return isPawnMove(chess, hasAlreadyMove, cd, ld, ca, la); break;
        default:
            return 0; break;
    }
}



/*-----------------------------------------------------------*/



/*Interaction entre le jeu et le joueur*/
int nextSquare(chessboard chess, char *c, int *l, int isSecondSquare) {
    int n = chess.taille;
    char buffer[5];
    char buffer1[3];
    char choix;
    
    printf("Quel est votre choix ?\n");
    if( isSecondSquare == 0 || isSecondSquare == 2 )
    {
        printf("Colonne (entre A et %c) et ligne (entre 1 et %i)\n", 'A' + n - 1, n);
        printf("ou abandonnner \"!\"\n");
        if( isSecondSquare == 2 )
            printf("ou changer de première case \"&\"\n");
        
        readKeyboard(buffer, 5);
        
        *c = buffer[0];
        if( *c == '!' ) /*abandon*/
            return -1;
        else if( *c == '&' )
            return 0;
        else
        {
            buffer1[0] = buffer[1];
            buffer1[1] = buffer[2];
            buffer1[2] = '\0';
            *l = strtol(buffer1, NULL, 10);
            /*
             char A = 65, char Z = 90;
             char a = 97, char z = 122;
             si *c est superieur à Z alors c'est peut être une lettre minuscule, on se ramène alors au majuscule*/
            if( *c > 'Z' )
                *c = *c - 'a' + 'A';
            return 1;
        }
    }
    else
    {
        do
        {
            printf("presse \"c\" pour changer de case\n");
            printf("presse \"d\" pour choisir une case d'arrivée\n");
            printf("presse \"a\" pour abandoner\n");
            
            readKeyboard(buffer, 5);
            choix = buffer[0];
            
            switch (choix)
            {
                case 'a':
                case 'A':
                    return -1;
                    break;
                case 'd':
                case 'D':
                    if( isGoodPlayerSquare(chess, *c, *l) == 1 ) /*La case appartient au bon joueur*/
                        return 1;
                    else
                    {
                        clearTerminal();
                        printf("\nLa case sélectionnée n'est pas valide : ");
                        printf("la case ne vous appartient pas !\n");
                        return -2;
                    }
                    break;
                case 'c':
                case 'C':
                    printf("\n");
                    return 0;
                default:
                    printf("\nErreur lors de la saisie !\n\n");
                    break;
            }
        } while( choix != 'c' && choix != 'd' && choix != 'a' && choix != 'C' && choix != 'D' && choix != 'A' );
        return -1;
    }
}



int isGoodPlayerSquare(chessboard chess, char c, int l) {
    if(chess.tour == BLANC && isWhiteCase(chess, c, l))
        return 1;
    else if(chess.tour == NOIR && isBlackCase(chess, c, l))
        return 1;
    else
        return 0;
}



int isSquareOK(chessboard chess, char c, int l) {
    int n = chess.taille;
    if( 0 < l && l <= n ) /*La ligne est correcte*/ {
        if ('A' <= c && c <= 'A' + n - 1) /*La colonne est correcte*/
        {
            
            return 1;
        }
        else {
            clearTerminal();
            printf("\nLa case sélectionnée n'est pas valide : ");
            printf("colonne invalide !\n");
            return -2;
        }
    }
    else {
        clearTerminal();
        printf("\nLa case sélectionnée n'est pas valide : ");
        printf("ligne invalide !\n");
        return -3;
    }
}



int nb_de_pieces_a_deplacer(int nbPiecesPremiereCase) {
    char buffer[4];
    int n = 0;
    int ok;
    
    do {
        printf("Combien de pieces voulez-vous déplacer ? ");
        ok = readKeyboard(buffer, 4);
        if( ok == 1 )
        {
            n = strtol(buffer, NULL, 10);
            if( n > nbPiecesPremiereCase || n <= 0 )
                printf("Erreur vous n'avez pas assez de pieces ou la saisie est incorrect !\n\n");
        }
        else
            printf("\nLa saisie est incorrecte !\n");
    } while( n > nbPiecesPremiereCase || n <= 0 || ok != 1 );
    
    return n;
}



int isOffensiveOK(chessboard chess, char cd, int ld, char ca, int la, int nbPiecesPremiereCase) {
    int nbPiecesSecondCase = nbPieces(chess, ca, la);
    int i;
    
    if( nbPiecesPremiereCase <= nbPiecesSecondCase )
    {
        clearTerminal();
        printf("\nVous n'avez pas sélectionné assez de pieces pour attaquer !\n");
        return 0;
    }
    else
    {
        stack tmp = *popCases(chess, cd, ld);
        
        if( isKnightMove(cd, ld, ca, la) == 1 )
        {
            int nbCavalier = 0;
            for(i = 0; i < nbPiecesPremiereCase; i++)
            {
                if( returnTop(tmp) == CB || returnTop(tmp) == CN )
                    nbCavalier++;
                tmp = tmp->next;
            }
            if( nbCavalier >= nbPiecesPremiereCase/2 )
                return 1;
            else
            {
                clearTerminal();
                printf("\nVous n'avez pas assez de cavalier !\n");
                return 0;
            }
        }
        else
        {
            for(i = 0; i < nbPiecesPremiereCase; i++)
            {
                if( isMoveLegal(chess, returnTop(tmp), hasAlreadyMove(tmp), cd, ld, ca, la) == 1 || isMoveLegal(chess, returnTop(tmp), hasAlreadyMove(tmp), cd, ld, ca, la) == 3 )
                    tmp = tmp->next;
                else
                {
                    clearTerminal();
                    printf("\nUne de vos pieces n'a pas le bon mouvement !\n");
                    return 0;
                }
            }
            return 1;
        }
    }
}



int isSecondSquareOK(chessboard chess, char cd, int ld, char ca, int la, int nbPiecesPremiereCase) {
    int ok = isSquareOK(chess, ca, la); /*La case existe-t-elle ?*/
    if( ok != 1 )
        return ok;
    
    if( ca == cd && la == ld ) /*Le joueur à entré deux fois la même case*/
    {
        clearTerminal();
        printf("\nVous avez choisi la même case !\n");
        return -1;
    }
    
    stack tmp = *popCases(chess, cd, ld);
    
    if( isEmptyCase(chess, ca, la) == 1 || isGoodPlayerSquare(chess, ca, la) == 1 ) /*la case d'arrivée est vide ou appartient au joueur actif*/
    {
        int i;
        if( isKnightMove(cd, ld, ca, la) == 1 )
        {
            int nbCavalier = 0;
            for(i = 0; i < nbPiecesPremiereCase; i++)
            {
                if( returnTop(tmp) == CB || returnTop(tmp) == CN )
                    nbCavalier++;
                tmp = tmp->next;
            }
            if( nbCavalier >= nbPiecesPremiereCase/2 )
                return 1;
            else
            {
                clearTerminal();
                printf("\nVous n'avez pas assez de cavalier !\n");
                return 0;
            }
        }
        else
        {
            for(i = 0; i < nbPiecesPremiereCase; i++)
            {
                if( isMoveLegal(chess, returnTop(tmp), hasAlreadyMove(tmp), cd, ld, ca, la) == 1 || isMoveLegal(chess, returnTop(tmp), hasAlreadyMove(tmp), cd, ld, ca, la) == 2 )
                    tmp = tmp->next;
                else
                {
                    clearTerminal();
                    printf("\nLe mouvement n'est pas valide !\n");
                    return 0;
                }
            }
            return 1;
        }
    }
    else
        return isOffensiveOK(chess, cd, ld, ca, la, nbPiecesPremiereCase);
}



void takePieces(player pl, int tabB[], int tabN[], stack* s) {
    piece p;
    
    while( isEmpty(*s) != 1 )
    {
        p = pop(s);
        if( pl == BLANC )
        {
            switch( p )
            {
                case PN:
                    tabN[5]--;
                    break;
                case RN:
                    tabN[1]--;
                    break;
                case DN:
                    tabN[2]--;
                    break;
                case FN:
                    tabN[3]--;
                    break;
                case TN:
                    tabN[4]--;
                    break;
                case CN:
                    tabN[0]--;
                    break;
                default:
                    break;
            }
        }
        else
        {
            switch( p )
            {
                case PB:
                    tabB[5]--;
                    break;
                case RB:
                    tabB[1]--;
                    break;
                case DB:
                    tabB[2]--;
                    break;
                case FB:
                    tabB[3]--;
                    break;
                case TB:
                    tabB[4]--;
                    break;
                case CB:
                    tabB[0]--;
                    break;
                default:
                    break;
            }
        }
    }
}



void move(chessboard chess, char cd, int ld, char ca, int la, int nbPiecesADeplacer, int tabB[], int tabN[]) {
    int i;
    int n = chess.taille;
    stack tmp;
    
    if( isEmptyCase(chess, ca, la) == 1 || isGoodPlayerSquare(chess, ca, la) == 1 )
    {
        for(i = 0; i < nbPiecesADeplacer; i++)
        {
            if( returnTopPieceCases(chess, cd, ld) == PB && la == n )
            {
                popPieceCases(chess, cd, ld);
                push(DB, &tmp, 0);
                tabB[2]++; /*le nb de dames augmente*/
                tabB[5]--; /*le nb de pions diminue*/
            }
            else if( returnTopPieceCases(chess, cd, ld) == PN && la == 1 )
            {
                popPieceCases(chess, cd, ld);
                push(DN, &tmp, 0);
                tabN[2]++; /*le nb de dames augmente*/
                tabN[5]--; /*le nb de pions diminue*/
            }
            else
                push(popPieceCases(chess, cd, ld), &tmp, 0);
        }
        for(i = 0; i < nbPiecesADeplacer; i++)
            pushPieceCases(chess, pop(&tmp), 1, ca, la);
    }
    else
    {
        takePieces(chess.tour, tabB, tabN, &*popCases(chess, ca, la));
        for(i = 0; i < nbPiecesADeplacer; i++)
        {
            if( returnTopPieceCases(chess, cd, ld) == PB && la == n )
            {
                popPieceCases(chess, cd, ld);
                push(DB, &tmp, 0);
                tabB[2]++;
                tabB[5]--;
            }
            else if( returnTopPieceCases(chess, cd, ld) == PN && la == 1 )
            {
                popPieceCases(chess, cd, ld);
                push(DN, &tmp, 0);
                tabN[2]++;
                tabN[5]--;
            }
            else
                push(popPieceCases(chess, cd, ld), &tmp, 0);
        }
        for(i = 0; i < nbPiecesADeplacer; i++)
            pushPieceCases(chess, pop(&tmp), 1, ca, la);
    }
    
}



int chooseSquare(chessboard chess, int tabB[], int tabN[]) {
    int nxt = 0; /*entier renvoyé par nextSquare(...)*/
    int ok; /*entier renvoyé par isSquareOK(...) et isSecondSquareOK(...)*/
    int nbPiecesPremiereCase; /*entier renvoyé par nbPiece(...)*/
    int nbPiecesADeplacer; /*entier renvoyé par nb_de_pieces_a_deplacer(nbPiecesPremiereCase)*/
    
    /*coordonnées*/
    char cd, ca; /*colonnes*/
    int ld, la; /*lignes*/
    
    do {
        do
        {
            if( nxt == 0 )
                clearTerminal();
            do
            {
                printChess(chess, NULL, NULL);
                if (chess.tour == BLANC)
                    printf("C'est aux Blancs de jouer ! ");
                else
                    printf("C'est aux Noirs de jouer ! ");
                nxt = nextSquare(chess, &cd, &ld, 0); /*on demande au joueur d'entrer une case*/
                if( nxt == -1 && chess.tour == BLANC ) /*abandon*/
                    return -1;
                else if( nxt == -1 && chess.tour == NOIR )
                    return -2;
                ok = isSquareOK(chess, cd, ld); /*La case existe-elle ?*/
            } while( ok != 1 );
            
            nbPiecesPremiereCase = nbPieces(chess, cd, ld); /*renvoi le nombre de piece dans la case demandé*/
            
            clearTerminal();
            printChess(chess, &cd, &ld);
            
            nxt = nextSquare(chess, &cd, &ld, 1); /*demande au joueur une action*/
            if( nxt == -1 && chess.tour == BLANC ) /*abandon*/
                return -1;
            else if( nxt == -1 && chess.tour == NOIR )
                return -2;
        } while( nxt != 1 ); /*tant que nxt n'est pas égale à 1 le joueur change de case ou abandonne*/
        
        clearTerminal();
        printChess(chess, &cd, &ld);
        nbPiecesADeplacer = nb_de_pieces_a_deplacer(nbPiecesPremiereCase); /*on demande au joueur combien de piece il veut déplacer*/
        clearTerminal();
        
        
        do
        {
            printChess(chess, &cd, &ld);
            nxt = nextSquare(chess, &ca, &la, 2); /*on demande au joueur d'entrer une case*/
            if( nxt == -1 ) /*abandon*/
                return -1;
            else if( nxt == 0 ) /*Le joueur veut changer le choix de sa première case*/
                break; /*on sort de la boucle while*/
            ok = isSecondSquareOK(chess, cd, ld, ca, la, nbPiecesADeplacer); /*est-ce que la case est correcte ?*/
        } while( ok != 1 ); /*tant que la seconde case n'est pas correcte*/
    } while( nxt == 0 ); /*tant que le joueur veut changer de première case*/
    
    
    move(chess, cd, ld, ca, la, nbPiecesADeplacer, tabB, tabN); /*on effectue le déplacement souhaité*/
    return 1;
}


