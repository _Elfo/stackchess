#include <stdio.h>
#include <stdlib.h>

#include "chessboardModel.h"


stack* popCases(chessboard chess, char c, int l) {
    return &chess.cases[l - 1][c - 65];
}



piece popPieceCases(chessboard chess, char c, int l) {
    return pop(popCases(chess, c, l));
}



piece returnTopPieceCases(chessboard chess, char c, int l) {
    return returnTop(*popCases(chess, c, l));
}



void pushPieceCases(chessboard chess, piece p, int hasMove, char c, int l) {
    push(p, popCases(chess, c, l), hasMove);
}



int nbPieces(chessboard chess, char c, int l) {
    int nb = 0;
    stack tmp = *popCases(chess, c, l);
    while(tmp != NULL) {
        nb++;
        tmp = tmp->next;
    }
    return nb;
}



int isEmptyCase(chessboard chess, char c, int l) {
    return isEmpty(*popCases(chess, c, l));
}



int isWhiteCase(chessboard chess, char c, int l) {
    return isWhitePiece(*popCases(chess, c, l));
}



int isBlackCase(chessboard chess, char c, int l) {
    return isBlackPiece(*popCases(chess, c, l));
}


/*-----------------------------------------------------------*/

void printLetter(int n) {
    int i;
    printf("   ");
    for(i = 65; i < 65 + n; i++)
        printf("%3c", i);
}



void printSeparation(int n) {
    int i;
    printf("   ");
    for(i = 65; i < 65 + n; i++)
        printf("+--");
    printf("+");
}



void printCases(chessboard chess, int i) {
    int j;
    int n = chess.taille;
    printf("%3i|", i);
    for(j = 0; j < n; j++)
    {
        printPiece(returnTop(chess.cases[i-1][j]));
        printf("|");
    }
}



/*Gestion de l'affichage*/
void printChess(chessboard chess, char *c, int *l) {
    int i;
    int n = chess.taille;
    int k = 2 * n + 1;
    
    if( c != NULL && l != NULL )
    {
        int nb_piece = nbPieces(chess, *c, *l);
        int nb = nb_piece;
        stack tmp = *popCases(chess, *c, *l);
        
        printf("\n");
        printLetter(n);
        printf("\n");
        for(i = n; i > 0; i--)
        {
            printSeparation(n);
            if( k - nb == 0 && nb > 2 )
            {
                printf("       ");
                printPiece(returnTop(tmp));
                tmp = tmp->next;
                nb--;
            }
            printf("\n");
            k--;
            
            printCases(chess, i);
            if( k - nb == 0 && nb > 1 )
            {
                printf("       ");
                printPiece(returnTop(tmp));
                tmp = tmp->next;
                nb--;
            }
            printf("\n");
            k--;
        }
        printSeparation(n);
        printf("  %c%i : ", *c, *l);
        if( nb_piece == 0 )
            printf("case vide");
        else
            printPiece(returnTop(tmp));
        printf("\n");
        printLetter(n);
        printf("\n\n");
        
    }
    else
    {
        printf("\n");
        printLetter(n);
        printf("\n");
        printSeparation(n);
        printf("\n");
        for(i = n; i > 0; i--) {
            printCases(chess, i);
            printf("\n");
            printSeparation(n);
            printf("\n");
        }
        printLetter(n);
        printf("\n\n");
    }
}



/*Initialisation du jeu*/
void initChessboard(int n, chessboard *c) {
    int i, j;
    c->cases = (stack **) malloc(n * sizeof(stack *));
    for(i = 0; i < n; i++) {
        c->cases[i] = (stack *) malloc(n * sizeof(stack));
        for(j = 0; j < n; j++)
            c->cases[i][j] = NULL;
    }
    c->tour = BLANC; /*Les blancs commencent*/
    c->taille = n;
}



void startGame(int n, chessboard *echiquier, int tabB[], int tabN[]) {
    int i;
    int min;
    
    min = n / 2;
    
    initChessboard(n, echiquier);
    
    for(i = 0; i < n; i++)
    {
        createStack(PB, &echiquier->cases[1][i]);
        createStack(PN, &echiquier->cases[n-2][i]);
        tabB[5]++;
        tabN[5]++;
    }
    
    for(i = min - 1; i > 0; i--)
    {
        if( (min - i) % 3 == 1 )
        {
            createStack(FB, &echiquier->cases[0][i-1]);
            createStack(FN, &echiquier->cases[n-1][i-1]);
            tabB[3]++;
            tabN[3]++;
        }
        else if( (min - i) % 3 == 2 )
        {
            createStack(CB, &echiquier->cases[0][i-1]);
            createStack(CN, &echiquier->cases[n-1][i-1]);
            tabB[0]++;
            tabN[0]++;
        }
        else
        {
            createStack(TB, &echiquier->cases[0][i-1]);
            createStack(TN, &echiquier->cases[n-1][i-1]);
            tabB[4]++;
            tabN[4]++;
        }
    }
    
    for(i = min + 1; i < n; i++)
    {
        if( (i - min) % 3 == 1 )
        {
            createStack(FB, &echiquier->cases[0][i]);
            createStack(FN, &echiquier->cases[n-1][i]);
            tabB[3]++;
            tabN[3]++;
        }
        else if( (i - min) % 3 == 2 )
        {
            createStack(CB, &echiquier->cases[0][i]);
            createStack(CN, &echiquier->cases[n-1][i]);
            tabB[0]++;
            tabN[0]++;
        }
        else
        {
            createStack(TB, &echiquier->cases[0][i]);
            createStack(TN, &echiquier->cases[n-1][i]);
            tabB[4]++;
            tabN[4]++;
        }
    }
    
    createStack(DB, &echiquier->cases[0][min-1]);
    createStack(DN, &echiquier->cases[n-1][min-1]);
    createStack(RB, &echiquier->cases[0][min]);
    createStack(RN, &echiquier->cases[n-1][min]);
    tabB[1]++;
    tabB[2]++;
    tabN[1]++;
    tabN[2]++;
}


/*-----------------------------------------------------------*/


/*Gestion des fuites mémoires*/
void freeChessboard(chessboard* c, int tabB[], int tabN[], int size) {
    int i, j;
    int n = c->taille;
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < n; j++)
            clearStack(&c->cases[i][j]);
        free(c->cases[i]);
    }
    free(c->cases);
    for(i = 0; i < size; i++)
    {
        tabB[i] = 0;
        tabN[i] = 0;
    }
}
